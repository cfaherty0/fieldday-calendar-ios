//
//  FDCalendar.swift
//  FDCalendar
//
//  Created by Chris Faherty on 5/25/21.
//

import SwiftUI

extension Color {
	init(hex: UInt, alpha: Double = 1) {
		self.init(
			.sRGB,
			red: Double((hex >> 16) & 0xff) / 255,
			green: Double((hex >> 08) & 0xff) / 255,
			blue: Double((hex >> 00) & 0xff) / 255,
			opacity: alpha
		)
	}
}

public enum FDApptDataType {
	case day
	case before
	case after
}

public struct FDApptData: Hashable {
	var type: FDApptDataType = FDApptDataType.day
	let day: Int
	let month: Int
	let year: Int
    public init(type: FDApptDataType = FDApptDataType.day, day: Int, month: Int, year: Int) {
        self.type = type
        self.day = day
        self.month = month
        self.year = year
    }
}

struct FDDayData: Hashable {
    var x: Int
	var y: Int
	var w: Int
	var h: Int
	var day: Int
	var month: Int
	var year: Int
	var textColor: Color
	var backgroundColor: Color
}

struct FDDayView: View, Identifiable {
	var id = UUID()
	var x: Int
	var y: Int
	var w: Int
	var h: Int
	var day: Int
	var month: Int
	var year: Int
	var textColor: Color
	var backgroundColor: Color
	var clickCallback: (Int, Int, Int) -> Void

	init(x: Int, y: Int, w: Int, h: Int, day: Int, month: Int, year: Int, textColor: Color, backgroundColor: Color, clickCallback: @escaping (Int, Int, Int) -> Void) {
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.day = day
		self.month = month
		self.year = year
		self.textColor = textColor
		self.backgroundColor = backgroundColor
		self.clickCallback = clickCallback
	}

	var body: some View {
		Text(String(self.day))
			.font(Font.body)
			.foregroundColor(self.textColor)
			.background(
				Circle()
					.fill(self.backgroundColor)
					.frame(width: CGFloat(self.w - 4), height: CGFloat(self.h - 4))
			)
			.frame(width: CGFloat(self.w), height: CGFloat(self.h))
			.position(x: CGFloat(self.x + self.w / 2), y: CGFloat(self.y + self.h / 2))
			.onTapGesture {
				clickCallback(self.day, self.month, self.year)
			}
	}
}

struct FDDayView_Previews: PreviewProvider {
	static var previews: some View {
		FDDayView(x: 0, y: 0, w: 50, h: 50, day: 1, month: 1, year: 2021, textColor: Color.black, backgroundColor: Color.red, clickCallback: { _, _, _ in })
	}
}

public struct FDMonthView: View {
	@Binding var showSheetView: Bool
	@State private var month: Int
	@State private var year: Int
	@Binding var notAvailable: [FDApptData]
	@Binding var selected: [FDApptData]
	@State private var newSelected: [FDApptData]
	var seeActivityTimesCallback: () -> Void
	private var multiSelection: Bool = true

	public init(showSheetView: Binding<Bool>, month: Int, year: Int, notAvailable: Binding<[FDApptData]>, selected: Binding<[FDApptData]>, seeActivityTimesCallback: @escaping () -> Void) {
		_showSheetView = showSheetView
		self.month = month
		self.year = year
		_notAvailable = notAvailable
		_selected = selected
		self.newSelected = _selected.wrappedValue
		self.seeActivityTimesCallback = seeActivityTimesCallback
	}

    public func setOptions(multiSelection: Bool) -> some View {
		var copy = self
		copy.multiSelection = multiSelection
		return copy
	}

	func createHeader(w: Int) -> some View {
		let xi: Int = w / 7
		let xs: Int = (w - xi * 7) / 2
		var x: Int = xs
		let bias = (w / 2) - (xi / 2)
		var xpos: Array<Int> = []
		for _ in 0...6 {
			xpos.append(x)
			x = x + xi
		}
		return ZStack {
			ForEach(xpos.indices, id: \.self) { dow in
				Text(dowLabel(day: dow))
					.offset(x: CGFloat(xpos[dow] - bias), y: 0)
					.font(Font.subheadline)
					.foregroundColor(Color(hex: 0xF6F4F1))
			}
		}
	}

    func createMonth(w: Int, h: Int) -> some View {
		let allDayData = drawMonth(month: self.month, year: self.year, w: w, h: h)
		return ZStack {
			ForEach(allDayData, id: \.self) { dayData in
				FDDayView(x: dayData.x, y: dayData.y, w: dayData.w, h: dayData.h, day: dayData.day, month: dayData.month, year: dayData.year, textColor: dayData.textColor, backgroundColor: dayData.backgroundColor, clickCallback: { day, month, year in
					// Update arrays
					toggleDayData(day: day, month: month, year: year)
				})
			}
		}
			.frame(height: CGFloat(h))
	}

	func createLegend(w: Int) -> some View {
		return HStack {
			Spacer(minLength: 4)
			HStack {
				Circle()
					.fill(Color(hex: 0xF6F4F1))
					.frame(width: 18, height: 18)
				Text("Available")
					.font(Font.subheadline)
					.foregroundColor(Color(hex: 0xF6F4F1))
			}
			Spacer()
			HStack {
				Circle()
					.fill(Color(hex: 0x5A7784))
					.frame(width: 18, height: 18)
				Text("Not Available")
					.font(Font.subheadline)
					.foregroundColor(Color(hex: 0xF6F4F1))
			}
			Spacer()
			HStack {
				Circle()
					.fill(Color(hex: 0xC3B4E2))
					.frame(width: 18, height: 18)
				Text("Selected")
					.font(Font.subheadline)
					.foregroundColor(Color(hex: 0xF6F4F1))
			}
			Spacer(minLength: 4)
		}
	}

	public var body: some View {
        NavigationView {
			GeometryReader { geometry in
				if (geometry.size.width > 0) {
					VStack {
						HStack (alignment: .center) {
							Button(action: {
								let p_m = prevMonth(month: self.month, year: self.year)
								self.month = p_m.m
								self.year = p_m.y
							}, label: {
								Image(systemName: "chevron.left")
									.font(.system(size: 22, weight: .medium))
									.foregroundColor(Color(hex: 0xF6F4F1))
									.padding([.leading, .trailing], 20)
							})
							Text("\(monthLabel(month: self.month)) \(String(self.year))")
								.font(Font.title3.weight(.bold))
								.foregroundColor(Color(hex: 0xF6F4F1))
								.frame(maxWidth: .infinity)
							Button(action: {
								let p_m = nextMonth(month: self.month, year: self.year)
								self.month = p_m.m
								self.year = p_m.y
							}, label: {
								Image(systemName: "chevron.right")
									.font(.system(size: 22, weight: .medium))
									.foregroundColor(Color(hex: 0xF6F4F1))
									.padding([.leading, .trailing], 20)
							})
						}
							.padding(.top, 20)
						Spacer()
						VStack {
							createHeader(w: Int(geometry.size.width - 20))
							createMonth(w: Int(geometry.size.width - 20), h: Int(geometry.size.width - 40))
						}
							.frame(width: CGFloat(geometry.size.width - 20))
							.gesture(DragGesture(minimumDistance: 20, coordinateSpace: .global)
								.onEnded { value in
									let horizontalAmount = value.translation.width as CGFloat
									let verticalAmount = value.translation.height as CGFloat
									if abs(horizontalAmount) > abs(verticalAmount) {
										if (horizontalAmount < 0) {
											let p_m = nextMonth(month: self.month, year: self.year)
											self.month = p_m.m
											self.year = p_m.y
										} else {
											let p_m = prevMonth(month: self.month, year: self.year)
											self.month = p_m.m
											self.year = p_m.y
										}
									}
								}
							)
						createLegend(w: Int(geometry.size.width))
						Spacer()
						Button(action: {
							self.seeActivityTimesCallback()
						}, label: {
							Text("See activity times")
								.font(Font.title3.weight(.bold))
								.foregroundColor(Color(hex: 0x4E7180))
								.frame(maxWidth: .infinity)
								.padding(15.0)
								.background(RoundedRectangle(cornerRadius: 30.0).fill(Color(hex: 0xF6F4F1)))
						})
							.padding(.bottom, 20)
							.frame(width: CGFloat(geometry.size.width - 20))
					}
				}
			}
				.background(Color(hex: 0x4E7180))
				.navigationBarTitle(Text(""), displayMode: .inline)
				.navigationBarItems(
					leading: Button(action: {
						self.showSheetView = false
					}) {
						Image(systemName: "chevron.left")
							.font(.system(size: 18, weight: .medium))
						Text("Cancel").bold()
					},
					trailing: Button(action: {
						self.selected = self.newSelected
						self.showSheetView = false
					}) {
						Text("Done").bold()
					})
		}
	}

	func drawMonth(month: Int, year: Int, w: Int, h: Int) -> [FDDayData] {
		var day_data: Array<FDDayData> = []
		var i = 0
		var day = 1
		let day_count = daysInMonth(month: month, year: year)
		let day_start = dow(day: 1, month: month, year: year)
		let p_m = prevMonth(month: month, year: year)
		let prev_day_count = daysInMonth(month: p_m.m, year: p_m.y)
		let prev_day_start = prev_day_count - day_start
		let next_day_start = day_count
		let n_m = nextMonth(month: month, year: year)
		var xi: Int = w / 7
		var yi: Int = h / 6
		let xs: Int = (w - xi * 7) / 2
		let ys: Int = (h - yi * 6) / 2
		var x: Int = xs
		var y: Int = ys
		for week in 0...5 {
			x = xs
			xi = w / 7
			for _ in 0...6 {
				i = i + 1
				var d_day: Int = 0
				var d_month: Int = 0
				var d_year: Int = 0
				if (i > day_start && day <= day_count) {
					d_day = day
					d_month = month
					d_year = year
					day = day + 1
				} else if (prev_day_start + i <= prev_day_count) {
					d_day = prev_day_start + i
					d_month = p_m.m
					d_year = p_m.y
				} else if (day > next_day_start) {
					d_day = day - next_day_start
					d_month = n_m.m
					d_year = n_m.y
					day = day + 1
				}
				let colors = getColors(day: d_day, month: d_month, year: d_year)
				let d = FDDayData(x: x, y: y, w: xi, h: yi, day: d_day, month: d_month, year: d_year, textColor: colors.textColor, backgroundColor: colors.backgroundColor)
				day_data.append(d)
				x = x + xi
			}
			if (week == 5) {
				yi = h - y
			}
			y = y + yi
		}
		return day_data
	}

	func getColors(day: Int, month: Int, year: Int) -> (textColor: Color, backgroundColor: Color) {
		if (self.checkDayData(dayData: self.notAvailable, day: day, month: month, year: year) != nil) {
			// not available
			return (Color(hex: 0xF6F4F1), Color(hex: 0x5A7784))
		}
		if (self.checkDayData(dayData: self.newSelected, day: day, month: month, year: year) != nil) {
			// selected
			return (Color(hex: 0x272828), Color(hex: 0xC3B4E2))
		}
		// available
		return (Color(hex: 0x272828), Color(hex: 0xF6F4F1))
	}

	func checkDayData(dayData: [FDApptData], day: Int, month: Int, year: Int) -> Array<Any>.Index? {
		let index = dayData.firstIndex(where: {
			if ($0.type == FDApptDataType.day) {
				return $0.year == year && $0.month == month && $0.day == day
			} else if ($0.type == FDApptDataType.before) {
				return year < $0.year ||
					(year == $0.year && month < $0.month) ||
					(year == $0.year && month == $0.month && day < $0.day) ||
					(year == $0.year && month == $0.month && day == $0.day)
			} else if ($0.type == FDApptDataType.after) {
				return year > $0.year ||
					(year == $0.year && month > $0.month) ||
					(year == $0.year && month == $0.month && day > $0.day) ||
					(year == $0.year && month == $0.month && day == $0.day)
			}
			return false
		});
		return index;
	}

	func toggleDayData(day: Int, month: Int, year: Int) {
		if let index = self.checkDayData(dayData: self.newSelected, day: day, month: month, year: year) {
    		// Already selected, unselect
			self.newSelected.remove(at: index)
			return
		} else if self.checkDayData(dayData: self.notAvailable, day: day, month: month, year: year) != nil {
			// Not available, do nothing
			return
		}
		if (!self.multiSelection) {
			self.newSelected.removeAll { _ in return true }
		}
		// Set as selected
		self.newSelected.append(FDApptData(day: day, month: month, year: year))
	}

	func daysInMonth(month: Int, year: Int) -> Int {
		let d = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
		var n = d[month - 1]
		if (month == 2 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
			n = n + 1
		}
		return n
	}

	func dow(day: Int, month: Int, year: Int) -> Int {
		let t = [0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4]
		let y = month < 3 ? year - 1 : year
		return Int((y + Int(y / 4) - Int(y / 100) + Int(y / 400) + t[month - 1] + day) % 7)
	}

	func dowLabel(day: Int) -> String {
		let labels = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
		return labels[day]
	}

	func monthLabel(month: Int) -> String {
		let labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		return labels[month - 1]
	}

	func prevMonth(month: Int, year: Int) -> (m: Int, y: Int) {
		return month > 1 ? (month - 1, year) : (12, year - 1)
	}

	func nextMonth(month: Int, year: Int) -> (m: Int, y: Int) {
		return month < 12 ? (month + 1, year) : (1, year + 1)
	}
}

struct FDMonthView_Previews: PreviewProvider {
	@State static var showSheetView: Bool = true
	@State static var notAvailable: [FDApptData] = []
	@State static var selected: [FDApptData] = []
	static var previews: some View {
        FDMonthView(showSheetView: self.$showSheetView, month: 1, year: 2020, notAvailable: self.$notAvailable, selected: self.$selected, seeActivityTimesCallback: {})
	}
}
