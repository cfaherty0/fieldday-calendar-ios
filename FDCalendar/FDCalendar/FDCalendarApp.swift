//
//  FDCalendarApp.swift
//  FDCalendar
//
//  Created by Chris Faherty on 5/25/21.
//

import SwiftUI

@main
struct FDalendarApp: App {
	@State var notAvailable: [FDApptData] = [
		FDApptData(day: 1, month: 1, year: 2021),
		FDApptData(type: FDApptDataType.before, day: 1, month: 11, year: 2020)
	]
	@State var selected: [FDApptData] = [
		FDApptData(day: 3, month: 1, year: 2021)
	]
 	@State private var showingSheet = false

	var body: some Scene {
		WindowGroup {
			Button("Show Sheet") {
				showingSheet.toggle()
			}
				.sheet(isPresented: $showingSheet) {
					FDMonthView(showSheetView: $showingSheet, month: 1, year: 2021, notAvailable: self.$notAvailable, selected: self.$selected, seeActivityTimesCallback: {
						// See activity times clicked
						showingSheet.toggle()
					})
						.setOptions(multiSelection: false)
				}
		}
	}
}
