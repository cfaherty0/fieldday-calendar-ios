Pod::Spec.new do |s|
    s.name             = 'FDCalendar'
    s.version          = '1.0.7'
    s.summary          = 'Field Day calendar utilities'
   
    s.description      = <<-DESC
  Field Day calendar utilities: FDMonthView
                         DESC
   
    s.homepage         = 'https://gitlab.com/cfaherty0/fieldday-calendar-ios'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { '<YOUR NAME HERE>' => '<YOUR EMAIL HERE>' }
    s.source           = { :git => 'https://gitlab.com/cfaherty0/fieldday-calendar-ios.git', :tag => s.version.to_s }
   
    s.platform = :ios, '14.1'
    s.ios.deployment_target = '14.1'
    s.source_files = 'FDCalendar/FDCalendar/FDCalendar.swift'
   
  end
