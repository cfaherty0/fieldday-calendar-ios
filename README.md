# Introduction

This is a template for getting started with iOS development using GitLab and [fastlane](https://fastlane.tools/).

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Blog post: Android publishing with iOS and fastlane](https://about.gitlab.com/2019/03/06/ios-publishing-with-gitlab-and-fastlane/)

# Getting started

```
import FDCalendar

struct CalendarApp: App {
	@State var notAvailable: [FDApptData] = [
		FDApptData(day: 1, month: 1, year: 2021),
		FDApptData(type: FDApptDataType.before, day: 1, month: 11, year: 2020)
	]
	@State var selected: [FDApptData] = [
		FDApptData(day: 2, month: 1, year: 2021),
		FDApptData(day: 3, month: 1, year: 2021),
		FDApptData(day: 4, month: 1, year: 2021)
	]
 	@State private var showingSheet = false

	var body: some Scene {
		WindowGroup {
			Button("Show Sheet") {
				showingSheet.toggle()
			}
				.sheet(isPresented: $showingSheet) {
					FDMonthView(showSheetView: $showingSheet, month: 1, year: 2021, notAvailable: self.$notAvailable, selected: self.$selected, seeActivityTimesCallback: {
						// See activity times clicked
						showingSheet.toggle()
					})
						.setOptions(multiSelection: false)
				}
		}
	}
}
```